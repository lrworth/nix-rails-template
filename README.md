# Usage

## Initial setup

1. Clone this repo somewhere, let's call it $APP.
2. `cd $APP && nix-shell` to start a shell with access to `rails`, `bundle`, and `bundix`, among other things.
3. `rm -rf .git` because you don't want history from _this_ repository.
4. `rails new . --skip-bundle --force` to create a Rails environment, but without installing the dependency gems -- Nix will do this soon.
5. Ensure these files are under version control: Gemfile, Gemfile.lock, gemset.nix, shell.nix.
6. Follow the instructions for updating Gemfile.

You now have a Rails project with gem dependencies managed by Nix.

## Updating Gemfile

If you make changes to Gemfile, then within the Nix shell:
1. `bundle lock --update` to generate Gemfile.lock from Gemfile.
2. `bundix` to generate gemset.nix from Gemfile.lock.
3. `exit` then `nix-shell`.

# Opinion

Bundix is weird:
* The documentation is lacking. gemset.nix is not entirely derived from Gemfile.lock: it also stores the sha256 hashes of the downloaded gem files, allowing for
  reproducibility guarantees beyond what is possible with Bundler alone. This is why it needs to be version controlled along with Gemfile and Gemfile.lock, and
  why `bundix` needs to be run when Gemfile.lock is updated.
* It is not clear if there is a difference between `bundix --lock` and `bundler lock && bundix`, but the latter lets you pass options to `bundler` so I prefer
  it.
* It is not clear what `bundix --magic` is for.
  
If I ever care enough, I may fix these problems.

