with (import <nixpkgs> {});

let
  rubyenv = bundlerEnv {
    name = "rb";
    # Setup for ruby gems using bundix generated gemset.nix
    inherit ruby;
    gemfile = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset = ./gemset.nix;
    # Bundler groups available in this environment
    groups = ["default" "development" "test" "production"];
  };
in stdenv.mkDerivation {
  name = "MyProject";
  version = "0.0.1";

  buildInputs = [
    stdenv
    git

    # Ruby deps
    ruby
    bundler

    bundix

    clang
    libxml2
    libxslt
    readline
    sqlite
    openssl
    libiconv

    rubyenv
  ];
}

